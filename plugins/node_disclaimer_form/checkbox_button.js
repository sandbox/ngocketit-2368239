/**
 * @file
 * Handles the click event of the checkbox on disclaimer form.
 */

(function($) {
  Drupal.behaviors.checkboxButton = {
    attach: function(context, settings) {
      $('#disclaimer-button').bind('click', function() {
        if ($('#disclaimer-checkbox').is(':checked')) {
          Drupal.NodeDisclaimer().acceptDisclaimer();
        }
      });
    }
  };

})(jQuery);

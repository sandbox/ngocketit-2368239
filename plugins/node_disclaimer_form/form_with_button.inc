<?php
/**
 * @file
 * A plugin that defines the disclaimer form with buttons.
 */

$plugin = array(
  'label' => t('Disclaimer form with buttons'),
  'handler' => array(
    'class' => 'NodeDisclaimerFormButton',
  ),
);


class NodeDisclaimerFormButton extends NodeDisclaimerForm {
  /**
   * Render the disclaimer form.
   */
  public function disclaimerForm() {
    $form = $this->defaultDisclaimerForm();

    $form['agree'] = array(
      '#type' => 'button',
      '#id' => 'disclaimer-accept',
      '#value' => t('Accept'),
    );

    return $form;
  }

  /**
   * Get the form element that triggers the acceptance.
   */
  public function getAcceptElement() {
    return array(
      'selector' => '#disclaimer-accept',
      'trigger'  => array('click'),
    );
  }
}

<?php

/**
 * @file
 * Definition of \Drupal\node_disclaimer\DisclaimerInterface.
 */

namespace Drupal\node_disclaimer;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provide an interface for a disclaimer config entity.
 */
interface DisclaimerInterface extends ConfigEntityInterface {

}

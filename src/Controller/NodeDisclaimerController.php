<?php

/**
 * @file
 * Contains \Drupal\node_disclaimer\NodeDisclaimerController.
 */

namespace Drupal\node_disclaimer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\Query\QueryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Component\Utility\String;
use Drupal\Core\Config\Entity\Query\Query;

/**
 * Controller routines for admin features of the module.
 */
class NodeDisclaimerController extends ControllerBase {
  /**
   * The Entity query object.
   *
   * @var \Drupal\Core\Entity\Query\QueryInterface
   */
  protected $entityQuery;


  /**
   * Constructs a NodeDisclaimerController object.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $entityQuery
   *   The entity query object used to query for entities.
   */
  public function __construct($entityQuery) {
    // @todo: Why putting QueryInterface type hint doesn't work?
    $this->entityQuery = $entityQuery;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query')
    );
  }


  /**
   * Handle the output of the autocomplete search.
   *
   * @param $nids array
   *   An array of result node ids.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response representing the output.
   */
  protected function handleAutocomplete($nids) {
    $nodes = $this->entityManager()->getStorage('node')->loadMultiple($nids);
    $retval = array();

    if (!empty($nodes)) {
      foreach ($nodes as $node) {
        $label = String::checkPlain($node->label());
        $nid = $node->id();
        $retval[] = array('value' => $label . " [$nid]", 'label' => $label);
      }
    }

    return new JsonResponse($retval);
  }


  /**
   * Performs the autocomplete search for disclaimer nodes.
   *
   * @return string
   *   JSON string representing search result.
   */
  public function disclaimerAutocomplete(Request $request) {
    // Get the existing disclaimer IDs (node IDs).
    $dids = $this->entityQuery->get('node_disclaimer')->execute();
    $text = $request->query->get('q');

    $query = $this->entityQuery->get('node')
      ->condition('status', 1)
      ->condition('title', $text, 'CONTAINS')
      ->condition('nid', $dids, 'NOT IN');

    $bundle_types = $this->getEnabledBundleTypes('disclaimer_types');

    if (!empty($bundle_types)) {
      $query->condition('type', $bundle_types, 'IN');
    }

    return $this->handleAutocomplete($query->execute());
  }


  /**
   * Perform the autocomplete search for protected nodes.
   *
   * @return string
   *   JSON string representing search result.
   */
  public function nodeAutocomplete() {
    $query = $this->entityQuery->get('node')
      ->condition('status', 1)
      ->condition('title', $text, 'CONTAINS');

    $bundle_types = $this->getEnabledBundleTypes('protected_types');

    if (!empty($bundle_types)) {
      $query->condition('type', $bundle_types, 'IN');
    }

    return $this->handleAutocomplete($query->execute());
  }


  /**
   * Get an array of enabled bundle types for disclaimer or protected nodes.
   *
   * @param $type string
   *   Either 'disclaimer_types' for disclaimer types or 'protected_types' for protected types.
   *
   * @return array
   *   An array of the enabled bundle types.
   */
  protected function getEnabledBundleTypes($type) {
    $config = $this->config('node_disclaimer.settings');
    $bundle_types = array();

    foreach ($config->get($type) as $bundle_type => $enabled) {
      if ($enabled) {
        $bundle_types[] = $bundle_type;
      }
    }

    return $bundle_types;
  }
}


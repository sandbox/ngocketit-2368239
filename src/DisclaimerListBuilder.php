<?php

/**
 * @file
 * Contains \Drupal\node_disclaimer\DisclaimerListBuilder.
 */

namespace Drupal\node_disclaimer;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

class DisclaimerListBuilder extends ConfigEntityListBuilder {
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('Name');
    $header['enabled'] = $this->t('Enabled');
    $header['validity'] = $this->t('Validity (days)');
    $header['form'] = $this->t('Form');
    $header['protected_nodes'] = $this->t('Protected nodes');

    return $header + parent::buildHeader();
  }


  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $controller = \Drupal::entityManager()->getStorage('node');
    $node = $controller->load($entity->nid);

    if ($node) {
      $row['name']['data'] = array(
        '#type' => 'link',
        '#title' => $node->label(),
        '#url' => $node->urlInfo(),
      );
    }
    else {
      $row['name'] = $this->t('N/A');
    }

    $row['enabled'] = $entity->enabled ? $this->t('Yes') : $this->t('No');
    $row['validity'] = $entity->validity;
    $row['form'] = $entity->form_name;
    $row['protected_nodes'] = count($entity->protected_nodes);

    return $row + parent::buildRow($entity);
  }
}

<?php

/**
 * @file
 * Contains \Drupal\node_disclaimer\Plugin\DisclaimerFormPluginManager.
 */

namespace Drupal\node_disclaimer\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Disclaimer form plugin manager.
 */
class DisclaimerFormPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct($subdir, \Traversable $namespace, ModuleHandlerInterface $module_handler, $plugin_interface) {

  }
}

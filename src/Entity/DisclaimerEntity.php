<?php

/**
 * @file
 * Contain \Drupal\node_disclaimer\Entity\DisclaimerEntity.
 */

namespace Drupal\node_disclaimer\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\node_disclaimer\DisclaimerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Define a disclaimer configuration entity class.
 *
 * @ConfigEntityType(
 *   id = "node_disclaimer",
 *   label = @Translation("Disclaimer"),
 *   fieldable = FALSE,
 *   handlers = {
 *     "view_builder" = "Drupal\node_disclaimer\DisclaimerViewBuilder",
 *     "list_builder" = "Drupal\node_disclaimer\DisclaimerListBuilder",
 *     "form" = {
 *       "add" = "Drupal\node_disclaimer\Form\DisclaimerForm",
 *       "edit" = "Drupal\node_disclaimer\Form\DisclaimerForm",
 *       "delete" = "Drupal\node_disclaimer\Form\DisclaimerDeleteForm"
 *     }
 *   },
 *   config_prefix = "disclaimer",
 *   admin_permission = "administer nodes",
 *   entity_keys = {
 *     "id" = "nid"
 *   },
 *   links = {
 *     "edit-form" = "node_disclaimer.edit_disclaimer",
 *     "delete-form" = "node_disclaimer.delete_disclaimer"
 *   }
 * )
 */
class DisclaimerEntity extends ConfigEntityBase implements DisclaimerInterface {
  use StringTranslationTrait;

  /**
   * The ID of the disclaimer.
   *
   * @var string
   */
  public $nid;


  /**
   * The enabled/disabled state of the disclaimer.
   *
   * @var bool
   */
  public $enabled;


  /**
   * The validity period of the disclaimer.
   *
   * @var number
   */
  public $validity;


  /**
   * The name of the form plugin of the disclaimer.
   *
   * @var string
   */
  public $form_plugin;


  /**
   * Settings of the disclaimer.
   *
   * @var array
   */
  public $settings = array();


  /**
   * List of the protected node IDs.
   *
   * @var array
   */
  public $protected_nodes = array();

  /**
   * {@inheritdoc)
   */
  public function id() {
    return $this->nid;
  }


  /**
   * {@inheritdoc}
   */
  public function label() {
    if ($this->isNew()) {
      return $this->t('N/A');
    }
    $storage = $this->entityManager()->getStorage('node');
    $node = $storage->load($this->nid);
    return $node ? $node->label() : $this->t('N/A');
  }
}
